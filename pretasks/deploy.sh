#!/bin/bash
#
# This bash script do pre-tasks of homework1 
# of the course CC422.
#
# PRE-TASKS:
# - Create SSH and RDP security groups
# - Create a EC2 key pair
#
# NOTE:
# - All resources are deployed on the default region
# - Security groups are created inside the default VPC

##### PRE-TASk: Create SSH and RDP security groups #####

echo "[*] Running PRE-TASKS script"

# Getting ID of default VPC
default_vpc=$(aws ec2 describe-vpcs \
				--query 'Vpcs[?IsDefault==`true`]' \
				| grep VpcId | cut -d '"' -f4)

echo "Default VPC: $default_vpc"

# Creating SSH security group	
ssh_sg=$(aws ec2 describe-security-groups \
          --group-names allow_ssh 2> /dev/null \
          | grep GroupId | cut -d '"' -f4)

if [[ -z $ssh_sg ]]; then
  echo "Creating SSH security group"
  ssh_sg=$(aws ec2 create-security-group \
        --description 'SSH Security Group' \
        --group-name allow_ssh \
        --vpc-id $default_vpc  \
        --tag-specifications 'ResourceType=security-group,Tags=[{Key=Name, Value=allow_ssh}]' \
        | grep 'GroupId' | cut -d '"' -f4)
fi

echo "SSH security group: $ssh_sg"

# Adding ingress to SSH security group
echo "Adding 22/tcp from 0.0.0.0/0 ingress to SSH security group"
aws ec2 authorize-security-group-ingress \
    --group-id $ssh_sg \
    --protocol tcp \
    --port 22 \
    --cidr 0.0.0.0/0

# Creating RDP security group	

rdp_sg=$(aws ec2 describe-security-groups \
          --group-names allow_rdp 2> /dev/null \
          | grep GroupId | cut -d '"' -f4)

if [[ -z $rdp_sg ]]; then 
  echo "Creating RDP security group"    
  rdp_sg=$(aws ec2 create-security-group \
        --description 'RDP Security Group' \
        --group-name allow_rdp \
        --vpc-id $default_vpc  \
        --tag-specifications 'ResourceType=security-group,Tags=[{Key=Name, Value=allow_rdp}]' \
        | grep 'GroupId' | cut -d '"' -f4)
fi

echo "RDP security group: $rdp_sg"

# Adding ingress to RDP security group
echo "Adding 3389/tcp from 0.0.0.0/0 ingress to RDP security group"
aws ec2 authorize-security-group-ingress \
    --group-id $rdp_sg \
    --protocol tcp \
    --port 3389 \
    --cidr 0.0.0.0/0


##### PRE-TASk: Create a EC2 key pair #####
KEYNAME='homework1'

# Verifing if homework1 key exist
key_pair=$(aws ec2 describe-key-pairs --key-names $KEYNAME)

if [[ -z $key_pair ]]; then
  echo "Creating $KEYNAME key pair"
  aws ec2 create-key-pair \
    --key-name $KEYNAME | jq -r '.KeyMaterial' > $KEYNAME.pem

  echo "Key-Pair Location: $PWD/$KEYNAME.pem"
fi

echo "[+] PRE-TASKS DONE!!"