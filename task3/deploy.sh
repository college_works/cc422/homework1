#!/bin/bash
#
# This bash script do task1 of homework1 
# of the course CC422.
#
# TASK 1:
# - Create an EC2 instance with Windows OS
#
# NOTE:
# - All resources are deployed on the default region and VPC


KEYNAME='homework1'
INSTANCE_TYPE='t2.micro'


# Linux Instance AMI
ami='ami-06e46074ae430fba6' # region us-east-1

echo "OS AMI: $ami"

# Getting id of RDP security group
ssh_sg=$(aws ec2 describe-security-groups \
			--query 'SecurityGroups[?GroupName==`allow_ssh`]' \
			| grep 'GroupId' | cut -d '"' -f4)


# Selecting the first subnet of default VPC (just for simplicity)
default_vpc=$(aws ec2 describe-vpcs \
				--query 'Vpcs[?IsDefault==`true`]' \
				| grep VpcId | cut -d '"' -f4)

subnetId=$(aws ec2 describe-subnets \
			--filter "Name=vpc-id,Values=$default_vpc" \
			--query "Subnets[].SubnetId" \
			| jq '.[0]' | cut -d '"' -f2)

echo "subnetId: $subnetId"
			

# Launch linux EC2 instance
echo "Lunching Linux EC2 instance"
instanceId=$(aws ec2 run-instances --image-id $ami \
              --instance-type $INSTANCE_TYPE \
              --key-name $KEYNAME \
              --security-group-ids $ssh_sg \
              --subnet-id $subnetId \
              | grep InstanceId |  cut -d '"' -f4)

echo "Instance information (InstanceId: $instanceId):"
aws ec2 describe-instances --instance-ids $instanceId \
    | grep -E "KeyName|PublicIpAddress|Platform"
	
	
	
	
	
	