#!/usr/bin/env python3
#
# This python script do task2 of homework1 
# of the course CC422.
#
# TASK 1:
# - Create two EC2 instances with Windows OS
#
# NOTE:
# - All resources are deployed on the default region and VPC

import boto3
import time

windows_ami='ami-0bde1eb2c18cb2abe' # region us-east-1

ec2 = boto3.client('ec2')

# RDP security group
allow_rdp = ec2.describe_security_groups(GroupNames=['allow_rdp'])

allow_rdp_id = allow_rdp['SecurityGroups'][0]['GroupId']
print(f"RDP group ID: {allow_rdp_id}")

# Creating two Windows instances
resources = ec2.run_instances(
    ImageId = windows_ami,
    InstanceType = 't2.micro',
    KeyName = 'homework1',
    MinCount = 2,
    MaxCount = 2,
    SecurityGroupIds = [allow_rdp_id]
)
instance_ids = [instance['InstanceId'] for instance in resources['Instances']]

print(f"Instances IDS: {' '.join(instance_ids)}")

print("====== SUMMARY ======")
s = 10
print(f"Sleep {s} ...")
time.sleep(s)

reservations = ec2.describe_instances(InstanceIds=instance_ids)['Reservations']

for r in reservations:
    for instance in r['Instances']:
      print(f"Instance {instance['InstanceId']}:")
      fields = ['KeyName', 'Platform', 
                'PublicIpAddress', 'PrivateIpAddress']
      for field in fields:
          print(f"\t{field}: {instance[field]}")
    
