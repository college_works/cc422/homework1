terraform {
  required_providers {
    aws = {
      source  = "hashicorp/aws"
      version = "~> 4.0"
    }
  }
}

provider "aws" {
  region = "us-east-1"
}


data "aws_vpc" "default" {
  default = true
}


data "aws_security_group" "allow_ssh" {
  name = "allow_ssh"
}

data "aws_security_group" "allow_rdp" {
  name = "allow_rdp"
}

# instances
resource "aws_instance" "windows" {
  ami           = "ami-0bde1eb2c18cb2abe" # us-east-1
  instance_type = "t2.micro"
  key_name      = "homework1"
  vpc_security_group_ids = [data.aws_security_group.allow_rdp.id]

  tags = {
    Name = "Windows-instance"
  }
}

output "windows_public_ip" {
  value = aws_instance.windows.public_ip
}


resource "aws_instance" "linux" {
  ami           = "ami-06e46074ae430fba6"
  instance_type = "t2.micro"
  key_name      = "homework1"
  vpc_security_group_ids = [data.aws_security_group.allow_ssh.id]

  tags = {
    Name = "Linux-instance"
  }
}

output "linux_private_ip" {
  value = aws_instance.windows.private_ip
}